var express = require('express');
var dotenv = require('dotenv');
const serveStatic = require('serve-static');
var path = require('path');
// var mongodb = require('mongoose');

var app = express();

// mongodb.connect('mongodb://lokendra:lokendra@ds115166.mlab.com:15166/stories', function(err) {
// 	if (err) {
// 		throw err;
// 	}
// });

dotenv.config({
	path: ".env"
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.use("/", serveStatic(path.join(__dirname, '/public')))


app.get("*", function(req, res, next) {
	res.sendFile(__dirname + '/public/index.html');
});

// Create default port to serve the app on
const port = process.env.PORT || 8080
app.listen(port)

console.log('listening on ' + port);
