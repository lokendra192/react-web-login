import React, { Component } from 'react';
import ReactTooltip         from 'react-tooltip';
import {Animated}           from "react-animated-css";

// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   Link 
// }                           from 'react-router-dom';

// import {LoginApi} from '../apis';

// import {
//   KittenTheme
// } from '../config/theme';


export default class Bankstatement extends Component {
  constructor(props){
    super(props);
    this.state = {
      loading : false
    }

    this.onSubmitHandler = this.onSubmitHandler.bind(this);
  }

  onSubmitHandler(){
    this.setState({loading : true});
    try{
      // let data = LoginApi(this.state.phone);
      // let response = fetch('https://www.galaxycard.in/screen', {})
      // console.log(response.status);
      this.setState({loading : false});
      this.props.history.push('/password');
    }catch(e){
      console.log(e);
    }
  }

  render () {
    return (
      <Animated animationIn="fadeIn" animationOut="jello" animationInDelay={100} animateOnMount={true} isVisible={true} className="appChild text-center">
        <h4 className="text-center"> Upload BankStatement Pdfs </h4>
        <input className="form-control form-group" type="file" placeholder="Upload Bank Statement"/>
        <button onClick={x=>{this.props.history.goBack()}} className="btn btn-dark" > go back </button>
        <button onClick={this.onSubmitHandler} className="btn btn-primary btn-large" > Upload </button>
      </Animated>
    )
  }

}
