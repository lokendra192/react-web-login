import React from 'react'

import Base from './base'
import Loader from 'react-loader-spinner'

import App, { SyncApi,SyncApi1, PersonalDataApi, PanApi, GmailTokenApi, AddAadhaarApi, SendAadharDetails, SetPasswordApi } from '../apis'

import { PersonalData } from '../components/PersonalData'
import { Pan } from '../components/Pan'
import { BankStatement } from '../components/BankStatement'
import { Aadhaar } from '../components/Aadhaar'
import { Password } from '../components/Password'

// import Onboarding from '../entities/Onboarding'
// import OnboardingMember from './OnboardingMember'

export default class OnboardingScreen extends Base {

  constructor(props) {
    super(props)
    this.state = {
      items: [],
      data : [],
      count: 0,
      statements_found: null,
      visible: false,
      already : false,
      screen : 'personal',
      alertCount : 0,
      ...this.state
    }

    this.adddata();
    this.adddata = this.adddata.bind(this);
    // this.animatedValue = new Animated.Value(0)
  }

  adddata(){
    let response = this.sync1();
    response.then(res => {
      // console.log(res.onboarding);
      let items = res.onboarding;

      var joined = this.state.items.concat(items);
      // var joined1 = this.state.data.concat(res);
      this.setState({items : items});
    });
  }

  componentDidMount(){
    // let response = this.sync1();
    // response.then(res => {
    //   // console.log(res.onboarding);
    //   let items = res.onboarding;
    //   var joined = this.state.items.concat(items);
    //   this.setState({items : joined});
    // });
  }

  // updateItems(){
  //   let response = this.sync1();
  //   // response.resolve();
  //   // console.log(response);
  //   response.then(res => {
  //     console.log(res);
  //   });
  //   // console.log(response);
  // }

  async loadState () {

  }

  // componentDidMount () {
  // super.componentDidMount()
  // this.props.navigation.setParams({
  //   logout: () => this.logout(),
  //   support: () => this.props.navigation.navigate('Support')
  // })
  // }

  componentDidAppear() {
    super.componentDidAppear()
    this.sync().then(() => this.loadState())
    this.loadState()
    this.setState({visible: true})
  }

  componentDidDisappear() {
    super.componentDidDisappear()
    this.setState({visible: false})
  }

  animateScreen (state) {
    // this.animatedValue.setValue(0)
    // Animated.timing(this.animatedValue, {
    //   toValue: 1,
    //   duration: 100,
    //   easing: Easing.cubic
    // }).start(() => {
      this.setState(state)
    // })
  }

  async update () {
    await this.sync()
    // await this.loadState()
  }

  _getItem (type) {
    // console.log('i ran ');
    // console.log(this.state.items[index]);
    switch (type) {
    // switch (this.state.items[index].type) {
      case 'personal':
        return <PersonalData loading={this.state.loading} onDone={async (data) => {
          console.log(data);
          if (!this.state.loading) {
            this.setState({loading: true})
            try {
              await this.makeApiCall(PersonalDataApi, data.name, data.email, data.referrer, data.from_where, data.income_type, data.income_range, data.married, data.kids, data.cibil, data.default_recent)
              // console.log(data);
              // time to go to next one!
              await this.update()
              this.adddata();
              this.setState({loading: false , screen : 'pan'})
            } catch (e) {
              alert("Sorry try again or Refresh the page")
              this.setState({loading: false})
              throw e
            }
          }
        }}/>
      case 'aadhaar':
        // return <h1> Aadhar screen </h1>;
        return <Aadhaar loading={this.state.loading} onSubmit={async (aadhaar, name, cookie, csrfToken , otp) => {
          try {
            this.setState({loading: true})
            await this.makeApiCall(AddAadhaarApi, aadhaar, name, cookie, csrfToken, otp)
            await this.update()
            this.adddata();
            console.log(this.state.items);
            this.setState({loading: false})
          } catch (e) {
            this.setState({loading: false})
            throw e
          }
        }} />
      case 'pan':
        return <Pan loading={this.state.loading} onDone={async ({pan, dob}) => {
          if (!this.state.loading) {
            this.setState({loading: true})
            try {
              // debugger;
              await this.makeApiCall(PanApi, pan, dob)
              // time to go to next one!
              await this.update()
              this.adddata();
              this.setState({loading: false , screen : 'bank_statement'})
            } catch (e) {
              // console.log(e.message);
              this.setState({loading: false})
              throw e
            }
          }
        }}/>
      case 'bank_statement':
        return <BankStatement loading={this.state.loading} count={this.state.alertCount} statements_found={this.state.statements_found} onSubmitToken={async (token) => {
          if (!this.state.loading) {
            // this.setState({loading: true})
            try {
              let data = await this.makeApiCall(GmailTokenApi, token)
              if (data) {
                this.setState({statements_found: data.count})
                // this.adddata();
                // await this.update()
              }
              // if(this.statements_found > 0){
                this.adddata();
              // }
              // this.setState({loading: false , screen : 'password'})
            } catch (e) {
              alert("Sorry try again or Refresh the page")
              this.setState({loading: false})
              throw e
            }
          }
        }} onTimer={async () => {
          try {
            // await this.update()
            // await this.adddata()
          } catch (e) {
          }
        }} />
      case 'password':
        return <Password onSubmit={async (code, confirmCode) => {
          await SetPasswordApi(code, confirmCode)
          this.adddata();
          await this.update()
        }} />
      case 'submitted':
        return <div><h1> Hello You have already submitted your details <br/> Thank You </h1> please download our App </div>
    }
  }

  _renderItem (index) {
    // console.log(this.state.items[index]);
    // console.log(index);
    if (this.state.items[index]) {
      // let {width, height} = Dimensions.get('window')
      return (
        <div>
          {this._getItem(index)}
        </div>
      )
    }
    else{
      return (
        <div>
          {this._getItem(index)}
        </div>
      )
    }
  }

  render() {

    // at any given time we will have a maximum of 2 children rendered. Elements 0 and 1 of the incomplete list
    // The second child will be rendered when first is being removed
    // this.adddata();

    let view;
    let carry = false;
    let carry1 = false;
    let viewCount = false;
    let success;
    let length = this.state.items.length;

    // if(length === 0){
    //   // this.setState({already : true})
    //   // console.log('empty array ' , length);
    //   // view = 'submitted';
    // }else{
    // console.log(this.state.items);

    this.state.items.forEach((x,i)=>{
      if(!x.complete && carry == false) {
        // if(x.type !== 'aadhaar'){
          view = x.type;
          carry = true;
        // }
      }
      if(i === 4 && carry == false) view = 'submitted';
      // console.log(i == 4);
    })

    // this.state.items.forEach(x=>{
    //   if(x.complete && carry1 == false){
    //     viewCount = true;
    //     carry1 = true;
    //   }
    // })

    // console.log('no. of views ' + viewCount);

    // if(viewCount === true){
    //   view = 'submitted';
    // }

    // }
    // if(!view) success =  "submitted";
    // view = 'password';

    // if(view == 'bank_statement'){
    //   this.setState({alertCount : 1});
    // }

    console.log(view);
    // view = 'bank_statement';
    // view = 'pan';
    // view = 'personal';
    return (
      <div className="p-4 m-4 pt-0 mt-0">
        {this._getItem(view)}
      </div>
    )
  }
}
