import React, { Component } from 'react';
import ReactTooltip         from 'react-tooltip';
import {Animated}           from "react-animated-css";

// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   Link 
// }                           from 'react-router-dom';

// import {LoginApi} from '../apis';

// import {
//   KittenTheme
// } from '../config/theme';


export default class Success extends Component {
  constructor(props){
    super(props);
    this.state = {
      
    }
  }

  render () {
    return (
      <Animated animationIn="fadeIn" animationOut="jello" animationInDelay={200} animateOnMount={true} isVisible={true} className="container text-center">
        <h3>Congratulations you are Successfully Registered on GalaxyCard Website</h3>
        <p><small> Make sure to Download our app to get Credit </small></p>
      </Animated>
    )
  }

}
