import React, { Component } from 'react';
import ReactTooltip         from 'react-tooltip';
import {Animated}           from "react-animated-css";

import {
  BrowserRouter as Router,
  withRouter
} from 'react-router-dom';

import OnboardingScreen from './onboarding';

import keys from '../keys';

import './otp.scss';
import Base from './base';

import { OtpApi , UpdateFcmTokenApi } from '../apis';

export default class Otp extends Base {

  constructor(props){
    super(props);
    this.state = {
      otp : "100387",
      onboarding : false,
      loading : false
    }
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this._handleClick = this._handleClick.bind(this);
  }

  async  onSubmitHandler(){
    this.setState({loading: true})
    console.log(this.state.otp)
      try {
        let data = await this.makeApiCall(OtpApi, this.props.phone, this.state.otp)
        // console.log("otp data" , data);
        // console.log(data);
        if (data.token) {
          keys.token = data.token;
          // keys.token = 'fe30d89d-5b56-47ca-8418-04219cf3465b'
          // store the token, and call sync
          // await RNSecureKeyStore.set('token', data.token)
          // if(Platform.OS !== "web"){
          // const token = await firebase.messaging().getToken()
          // if (token) {
          //   try{
          //     this.makeApiCall(UpdateFcmTokenApi, token)
          //   }catch(e){
          //     console.log('error in UpdateFcmTokenApi '  + e.message);
          //   }
          // }
          // }

          // var device = new Date().getTime();
          // var device = "1530721068048"

          await this.sync()
          // this.setState({loading: false , onboarding : true })
          this.props.history.push('./onboarding')

          // this.props.onDone=("onboarding");
          // this.props.navigation.navigate('Home')
        } else {
          let title, message, button
          if (data.code === 'invalid_otp') {
            title = 'Incorrect OTP'
            message = 'Unfortunately the OTP seems to be incorrect'
            button = 'Retry'
          } else {
            title = 'Error'
            message = 'Uh-oh! We could not verify your OTP. Please try again'
            button = 'OK'
          }
          alert(title, message, [
            {text: button, onPress: () => this.setState({otp: ''})}
          ], {
            cancelable: false
          })
          this.setState({loading: false})
        }
      } catch (e) {
        console.log(e.message);
        alert('Error', 'An error occured trying to verify your OTP', [
          {text: 'OK', onPress: () => this.setState({otp: ''})}
        ], {
          cancelable: false
        })
        this.setState({loading: false})
      }
  }

  _handleClick() {
    let mountNode = React.findDOMNode(this.refs.wassup);
    let unmount = React.unmountComponentAtNode(mountNode);
    // console.log(unmount); // false
  }

  render () {
    return (
      <Animated animationIn="fadeIn" animationOut="jello" animationInDelay={100} animateOnMount={true} isVisible={true} className="otp text-center">
        <h4 className="text-center"> Enter Otp </h4>
        <input type="number" onChange={e=>{this.setState({otp : e.target.value})}} className="form-control form-group" type="text" placeholder="Enter Otp"/>
        <button onClick={this.onSubmitHandler} className="btn btn-primary btn-large" > Verify </button>
      </Animated>
    )
  }

}
