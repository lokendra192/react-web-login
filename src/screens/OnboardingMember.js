// import { Platform } from 'react-native'

import Base from './base'

import App, { UpdateFcmTokenApi } from '../apis'

import User from '../entities/User'

// import RNSecureKeyStore from 'react-native-secure-key-store'

// import {startLocationTracking, stopLocationTracking} from '../utils/LocationListener'

// let firebase
// if (Platform.OS !== 'web') {
//   firebase = require('react-native-firebase');
// }

export default class OnboardingMember extends Base {
  constructor(props) {
    super(props)
    this.state = {
      syncLoading: false,
      locationTracking: false,
      ...this.state
    }
  }

  componentDidMount () {
    super.componentDidMount()
    // if(Platform !== 'web'){
    //   this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(token => {
    //     this.makeApiCall(App.UpdateFcmTokenApi, token)
    //   })
    // }
  }

  componentWillUnmount() {
    super.componentWillUnmount()
    this.onTokenRefreshListener()
  }

  async _performMemberChecks () {
    let proceed = await super._performMemberChecks()
    if (proceed) {
      // do we have a token
      try {
        await RNSecureKeyStore.get('token')
        let user = await User.count()
        if (!user) {
          this.setState({syncLoading: true})
          await this.sync()
          this.setState({syncLoading: false})
        }
        // first check if the user has enabled location for us or not
        // TODO: Enable location once the problem is identified
        // let locationData = await this.locationPermissions()
        // if (locationData[0] !== 'always' || locationData[1] !== 'authorized') {
        //   this.props.navigation.navigate('Location')
        //   return false
        // } else {
        //   this.setState({locationTracking: true})
        // }
        if (Platform.OS === 'ios') {
          const enabled = await firebase.messaging().hasPermission()
          if (!enabled) {
            this.props.navigation.navigate('NotificationPermission')
            return false
          }
        }
      } catch (e) {
        this.logout()
        return false
      }
    }
    return proceed
  }
  
  shouldComponentUpdate (nextProps, nextState) {
    if (nextState.locationTracking) {
      startLocationTracking()
    }
    return true
    // return super.shouldComponentUpdate(nextProps, nextState)
  }

  componentDidAppear () {
    super.componentDidAppear()
    // this._performMemberChecks()
    if (this.state.locationTracking) {
      startLocationTracking()
    }
  }

  componentDidDisappear () {
    stopLocationTracking()
  }
}