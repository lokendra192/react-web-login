import React, { Component } from 'react';
import ReactTooltip         from 'react-tooltip';
import {Animated}           from "react-animated-css";
import keys from '../keys';
import uuid from 'uuid/v1';

import {
  BrowserRouter as Router,
  withRouter
} from 'react-router-dom';

import Otp from './otp';
import OnboardingScreen from './onboarding';

import Base from './base';
import { LoginApi } from '../apis';

// import keys from '../keys';
// import {
//   KittenTheme
// } from '../config/theme';

// export default function Login(props) {

//   function handler(){
//     this.setState({name : "loki"});
//   }

//   return <h1 onClick={handler}>Hello, {props.name}</h1>;
// }

// console.log(makeApiCall);

export default class Login extends Base {
  constructor(props){
    super(props);
    this.state = {
      phone : '9893984365',
      loading : false,
      otp : false
    }
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
  }

  async onSubmitHandler(){
    // console.log('i ran ');
    // debugger;
    keys.deviceId = uuid();
    console.log('i ran ' , keys.deviceId);
    this.setState({loading: true});
    try {
      // this.props.history.push('./onboarding');
      // console.log(LoginApi);
      // debugger;
      await this.makeApiCall(LoginApi, this.state.phone.replace(/^\+91/, ''))
      // console.log(data)
      // go to OTP screen
      this.setState({loading: false})
      // bugsnag.setUser(this.state.phone)
      this.setState({loading : false , otp : true});
      // this.props.navigation.navigate('OTP', {phone: this.state.phone})
      // Navigation.push(this.props.componentId, {
      //   component: {
      //     name: 'OTP',
      //     passProps: {phone: this.state.phone}
      //   }
      // })
    } catch(e) {
        console.log(e.message)
    }
  }


  render () {
    var view = <div><input onChange={e=>this.setState({phone : e.target.value})} className="form-control form-group" type="text" placeholder="Enter Your Mobile"/><button onClick={this.onSubmitHandler} className="btn round btn-primary btn-large" > {this.state.loding === true ? 'Loading...' : 'Let\'s Go'} </button></div>
    
    var otpView = "";

    if(this.state.otp) {
      view = <Otp phone={this.state.phone} history = {this.props.history} />
    }
    if(this.state.loding){
      loding = <Loading />
    }

    return (
      <Animated animationIn="fadeIn" animationOut="jello" animationInDelay={100} animateOnMount={true} isVisible={true} className="appChild text-center">
        <img className="mb-4" src='./images/logo_extended@2x.webp' />
        {view}
      </Animated>
    )
  }
}

