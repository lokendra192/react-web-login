import React, { Component } from 'react'

// import { AppState } from 'react-native'
// import Onboarding from '../entities/Onboarding'
// import Billing from '../entities/Billing'
// import User from '../entities/User'

import {SyncApi,SyncApi1, LoginApi} from '../apis'

// import Permissions from 'react-native-permissions'
// import RNSecureKeyStore from 'react-native-secure-key-store';

export default class Base extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...this.state
    }
  }

  async _performMemberChecks() {
    return true
  }

  componentDidAppear () {
    this._performMemberChecks()
  }

  componentDidDisappear () {

  }

  // componentDidMount() {
  //   AppState.addEventListener('change', (nextAppState) => this._handleAppStateChange(nextAppState))
  //   this._didFocus = this.props.navigation.addListener('willFocus', () => this.componentDidAppear())
  //   this._didBlur = this.props.navigation.addListener('willBlur', () => this.componentDidDisappear())
  // }

  // componentWillUnmount() {
  //   AppState.removeEventListener('change', (nextAppState) => this._handleAppStateChange(nextAppState))
  //   this._didFocus.remove()
  //   this._didBlur.remove()
  // }

  // _handleAppStateChange = (nextAppState) => {
  //   if (/inactive|background/.test(this.state.appState) && nextAppState === 'active') {
  //     this.componentDidAppear()
  //   } else {
  //     this.componentDidDisappear()
  //   }
  //   this.setState({appState: nextAppState})
  // }

  async sync() {
    // let res = await SyncApi();
    await this.makeApiCall(SyncApi);
  }

  async sync1(){
    return await this.makeApiCall(SyncApi1);
  }

  async makeApiCall(api, ...args) {
    try {
      // console.log(this);
      console.log('in makeApiCall');
      // console.log('args in base ' ,  args);
      return await api.apply(this, args)
    } catch (e) {
      if (e.message === 'logout') {
        console.log('inside base logout');
        // this.logout()
      } else if (e.message === 'upgrade') {
        console.log('inside base show UpgradeModal')
        // this.showUpgradeModal()
      } else {
        throw e
      }
    }
  }
}