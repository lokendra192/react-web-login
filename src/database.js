import SQLite from 'react-native-sqlite-storage'

SQLite.enablePromise(true)

let _connection

export default {
  connection: async () => {
    return new Promise((resolve, reject) => {
      if (_connection) {
        resolve(_connection)
      } else {
        SQLite.openDatabase({name: 'galaxycard.db', location: 'Library'}).then((db) => {
          _connection = db
          db.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS versions (version_id INTEGER PRIMARY KEY NOT NULL);', [], (tx) => {
              tx.executeSql('select version_id from versions order by version_id desc limit 1;', [], async (tx, result) => {
                let version = result && result.rows.length && result.rows.item(0).version_id || 0
                if (version < 1) {
                  try {
                    tx.executeSql('CREATE TABLE billing (balance double not null, carried_over double, current_limit double, last_pay_by bigint, pay_by bigint, ends_at bigint);')
                    tx.executeSql('CREATE TABLE categories (id bigint primary key, name varchar not null, sort_order int not null default 0);')
                    tx.executeSql('CREATE TABLE offers (id bigint primary key, intro varchar, title varchar, description text, merchant_name varchar, merchant_id bigint, top boolean, expires_at bigint, link varchar, sort_order int not null default 0);')
                    tx.executeSql('CREATE TABLE onboarding (id bigint primary key, type varchar not null, complete boolean default false);')
                    tx.executeSql('CREATE TABLE operators (id bigint primary key, name varchar not null, category varchar, special boolean default false, service_id bigint not null, sort_order int not null default 0);')
                    tx.executeSql('CREATE TABLE services (id bigint primary key, name varchar not null, code varchar not null, sort_order int not null default 0);')
                    tx.executeSql('CREATE TABLE user (id bigint primary key, name varchar, phone varchar not null, promo_code varchar not null, referral_bonus int default null, has_picture boolean default false);')
                    await tx.executeSql('insert into versions(version_id) values(1);')
                  } catch (e) {
                    return reject(e)
                  }
                }
                resolve(_connection)
              })
            })
          })
        }).catch((e) => {
          reject(e)
        })
      }
    })
  }
}