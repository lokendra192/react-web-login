// import {scale, scaleVertical} from '../utils/scale'

const Colors = {
  accent: '#ffa21a',
  primary: '#0079f3',
  darkerPrimary: '#0066CC',
  secondaryAccent: '#ff524c',
  success: '#3bd555',
  disabled: '#cacaca',

  foreground: '#333',
  alterForeground: '#777',
  inverseForeground: '#ffffff',
  secondaryForeground: '#555',
  hintForeground: '#999',
  highlight: '#bcbcbc',

  background: '#f3f3f3',
  alterBackground: '#ffffff',
  overlayBackground: '#00000057',
  neutralBackground: '#f2f2f2',
  fadedBackground:'#e5e5e5',

  border: '#f0f0f0',

  gradientBaseBegin: '#ffa21a',
  gradientBaseEnd: '#ff524c',
  
  // -----
  faded: '#e5e5e5',
  icon: '#c2c2c2',
  neutral: '#f2f2f2',

  info: '#19bfe5',
  warning: '#feb401',
  danger: '#ed1c4d',
};

const Fonts = {
  light: 'Lato-Light',
  regular: 'Lato-Regular',
  bold: 'Lato-Regular',
  logo: 'Calligraphr-Regular',
  title: 'Liberation Sans'
};

const FontBaseValue = scale(18);

export const KittenTheme = {
  name: 'light',
  colors: {
    activityIndicator: {
      foreground: Colors.inverseForeground,
      background: Colors.overlayBackground
    },
    cards: {
      background: Colors.alterBackground
    },
    navbar: {
      background: Colors.primary,
      tint: Colors.inverseForeground,
      statusBarBackground: Colors.darkerPrimary
    },
    control: {
      background: Colors.inverseForeground
    },
    button: {
      highlight: Colors.secondaryAccent,
      disabled: Colors.disabled
    },
    text: {
      base: Colors.foreground,
      secondary: Colors.secondaryForeground,
      accent: Colors.accent,
      inverse: Colors.inverseForeground,
      hint: Colors.alterForeground,
      brand: Colors.primary,
      secondaryAccent: Colors.secondaryAccent,
      disabled: Colors.disabled
    },
    input: {
      text: Colors.alterForeground,
      background: Colors.background,
      label: Colors.secondaryForeground,
      placeholder: Colors.secondaryForeground,
    },
    pin: {
      active: Colors.secondaryAccent,
      normal: Colors.accent
    },
    screen: {
      base: Colors.background,
      alter: Colors.alterBackground,
      secondaryAccent: Colors.secondaryAccent
    },
    border: {
      base: Colors.border,
      accent: Colors.accent,
      secondary: Colors.secondaryForeground,
      highlight: Colors.highlight,
    },
    slider: {
      leftTrack: Colors.accent,
      rightTrack: Colors.disabled,
      thumb: Colors.secondaryAccent
    },
    gradients: {
      base: [Colors.gradientBaseBegin, Colors.gradientBaseEnd]
    },
    progressBar: {
      background: Colors.fadedBackground,
      color: Colors.secondaryAccent
    },
    wheel: {
      titleBar: Colors.alterBackground,
      confirm: Colors.secondaryAccent,
      cancel: Colors.hint,
      title: Colors.brand,
      background: Colors.alterBackground,
      text: Colors.base
    }
  },
  fonts: {
    sizes: {
      h0: scale(32),
      h1: scale(26),
      h2: scale(24),
      h3: scale(20),
      h4: scale(18),
      h5: scale(16),
      h6: scale(15),
      p1: scale(16),
      p2: scale(15),
      p3: scale(15),
      p4: scale(13),
      s1: scale(15),
      s2: scale(13),
      s3: scale(13),
      s4: scale(12),
      s5: scale(12),
      s6: scale(13),
      s7: scale(10),
      base: FontBaseValue,
      small: FontBaseValue * .8,
      medium: FontBaseValue,
      large: FontBaseValue * 1.2,
      xlarge: FontBaseValue / 0.75,
      xxlarge: FontBaseValue * 1.6,
    },
    lineHeights: {
      medium: 18,
      big: 24
    },
    family: {
      regular: Fonts.regular,
      light: Fonts.light,
      logo: Fonts.logo,
      title: Fonts.title
    }
  }
};
