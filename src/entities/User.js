import Db from '../database'

export default {
  count: async () => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select count(1) as count from user;', [], (tx, results) => {
          resolve(results && results.rows.length && results.rows.item(0).count || 0)
        })
      })
    })
  },
  saveAll: async (user) => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('delete from user;')
        if (user) {
          tx.addStatement('insert into user (id, referral_bonus, promo_code, name, phone, has_picture) values(?, ?, ?, ?, ?, ?);', [user.id, user.referral_bonus, user.promo_code, user.name, user.phone, user.picture])
        }
      }).then(resolve).catch(reject)
    })
  },
  findOne: async () => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('select * from user', [], (tx, results) => {
          resolve(results && results.rows.length && results.rows.item(0))
        })
      }).catch(reject)
    })
  },
  truncate: async () => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('delete from user;')
      }).then(resolve).catch(reject)
    })
  }
}
