import Db from '../database'

export default {
  async incompleteCount () {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select count(1) as count from onboarding where complete = 0;', [], (tx, results) => {
          resolve(results && results.rows.length && results.rows.item(0).count || 0)
        })
      })
    })
  },
  async count () {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select count(1) as count from onboarding;', [], (tx, results) => {
          resolve(results && results.rows.length && results.rows.item(0).count || 0)
        })
      })
    })
  },
  async findAllIncomplete () {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select * from onboarding where complete = 0 order by id asc;', [], (tx, results) => {
          let rows = []
          for (let i = 0, len = results.rows.length; i < len; i++) {
            rows.push(results.rows.item(i))
          }
          resolve(rows)
        })
      }).catch(reject)
    })
  },
  async saveAll (onboardings) {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('delete from onboarding;')
        if (onboardings instanceof Array) {
          for (let i = 0, len = onboardings.length; i < len; i++) {
            let onboarding = onboardings[i]
            tx.addStatement('insert into onboarding (id, type, complete) values(?, ?, ?);', [i, onboarding.type, !!onboarding.complete])
          }
        }
      }).then(resolve).catch(reject)
    })
  },
  truncate: async () => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('delete from onboarding;')
      }).then(resolve).catch(reject)
    })
  }
}
