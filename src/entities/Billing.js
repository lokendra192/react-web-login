import Db from '../database'

export default {
  count: async () => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select count(1) as count from billing;', [], (tx, results) => {
          resolve(results && results.rows.length && results.rows.item(0).count || 0)
        })
      })
    })
  },
  findOne: async () => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select * from billing limit 1;', [], (tx, results) => {
          resolve(results && results.rows.length && results.rows.item(0) || {})
        })
      }).catch(reject)
    })
  },
  saveAll: async (billing) => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('delete from billing;')
        if (billing) {
          tx.addStatement('insert into billing (balance, carried_over, current_limit, last_pay_by, pay_by, ends_at) values(?, ?, ?, ?, ?, ?);', [billing.balance, billing.carried_over, billing.current_limit, billing.last_pay_by, billing.pay_by, billing.ends_at])
        }
      }).then(resolve).catch(reject)
    })
  },
  truncate: async () => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('delete from billing;')
      }).then(resolve).catch(reject)
    })
  }
}
