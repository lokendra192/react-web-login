import Db from '../database'

export default {
  async saveAll (services) {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('delete from operators;')
        tx.addStatement('delete from services;')
        if (services instanceof Array) {
          for (let i = 0, len = services.length; i < len; i++) {
            let service = services[i]
            tx.addStatement('insert into services (id, name, code, sort_order) values(?, ?, ?, ?);', [service.id, service.name, service.code, service.sort_order])
            if (service.operators instanceof Array) {
              let operators = service.operators
              for (let j = 0, len2 = operators.length; j < len2; j++) {
                let operator = operators[j]
                tx.addStatement('insert into operators (id, name, category, special, service_id, sort_order) values(?, ?, ?, ?, ?, ?);', [operator.id, operator.name, operator.category, operator.special, service.id, operator.sort_order])
              }
            }
          }
        }
      }).then(resolve).catch(reject)
    })
  },
  async findAll() {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select * from services order by sort_order asc;', [], (tx, results) => {
          let rows = []
          for (let i = 0, len = results.rows.length; i < len; i++) {
            rows.push(results.rows.item(i))
          }
          resolve(rows)
        })
      }).catch(reject)
    })
  }
}
