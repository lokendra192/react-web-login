import Db from '../database'

export default {
  saveAll: async (categories) => {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.addStatement('delete from categories;')
        if (categories instanceof Array) {
          for (let i = 0, len = categories.length; i < len; i++) {
            let category = categories[i]
            tx.addStatement('insert into categories (id, name, sort_order) values(?, ?, ?);', [category.id, category.name, category.sort_order])
          }
        }
      }).then(resolve).catch(reject)
    })
  }
}
