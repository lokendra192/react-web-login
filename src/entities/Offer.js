import Db from '../database'

export default {
  async saveAll (offers) {
    let connection = await Db.connection()
    if (offers instanceof Array) {
      return new Promise((resolve, reject) => {
        connection.transaction((tx) => {
          tx.addStatement('delete from offers;')
          for (let i = 0, len = offers.length; i < len; i++) {
            let offer = offers[i]
            tx.addStatement('insert into offers (id, merchant_name, merchant_id, top, intro, title, description, expires_at, link, sort_order) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);', [offer.id, offer.brand ? offer.brand.name : null, offer.brand ? offer.brand.id : null, offer.top, offer.intro, offer.title, offer.description, offer.expires_at, offer.link, i + 1])
          }
        }).then(resolve).catch(reject)
      })
    }
  },
  async findTop () {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select * from offers where top = ? order by sort_order asc', [1], (tx, results) => {
          let rows = []
          for (let i = 0, len = results.rows.length; i < len; i++) {
            rows.push(results.rows.item(i))
          }
          resolve(rows)
        })
      }).catch(reject)
    })
  }
}
