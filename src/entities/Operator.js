import Db from '../database'

export default {
  async findAll (query) {
    let connection = await Db.connection()
    return new Promise((resolve, reject) => {
      connection.transaction((tx) => {
        tx.executeSql('select * from operators where service_id = ?' + (query.category ? ' and category = ?' : '') + ' order by sort_order asc', query.category ? [query.serviceId, query.category] : [query.serviceId], (tx, results) => {
          let rows = []
          for (let i = 0, len = results.rows.length; i < len; i++) {
            rows.push(results.rows.item(i))
          }
          resolve(rows)
        })
      }).catch(reject)
    })
  }
}
