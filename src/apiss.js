// import RNSecureKeyStore from 'react-native-secure-key-store'

// import { Platform, StatusBar } from 'react-native'

// import Billing from './entities/Billing'
// import Category from './entities/Category'
// import Offer from './entities/Offer'
// import Onboarding from './entities/Onboarding'
// import Service from './entities/Service'
// import User from './entities/User'

// import { getLastLocation } from './utils/LocationListener'

// const DeviceInfo = require('react-native-device-info')

const JSON_HEADER = 'application/json'
// const API_BASE = __DEV__ ? 'http://192.168.1.181:3001' : 'https://api.galaxycard.in'
const API_BASE = "https://api.galaxycard.in"

const makeRequest = async (url, options) => {
  options = options || {}
  if (!options.full_url) {
    url = `${API_BASE}/v${options.version || 1}${url}`
  }
  options.headers = options.headers || {}
  // let location = getLastLocation()
  // if (location) {
  //   options.headers.lat = location.coords.latitude
  //   options.headers.lng = location.coords.longitude
  // }
  // try {
  //   options.headers['x-auth-token'] = await RNSecureKeyStore.get('token')
  // } catch (e) {}
  // try {
  //   options.headers.device = await RNSecureKeyStore.get('device')
  // } catch (e) {}

  
  options.headers['Content-Type'] = options.headers['Content-Type'] || JSON_HEADER
  let opts = {
    headers: options.headers,
    method: options.method || 'GET'
  }
  
  if (options.body) {
    opts.body = options.body
  }

  try {
    let response = await fetch(url, opts)
    if (response.status === 401) {
      throw new Error('logout')
    } else if (response.status === 426) {
      throw new Error('upgrade')
    }
    return response
  } catch (e) {
    throw e
  }
}

module.exports = {
  async LoginApi (phone) {
    let response = await makeRequest('/session', {
      method: 'POST',
      body: JSON.stringify({phone})
    })
    if ((response.status & 200) === 200) {
      try {
        return await response.json()                                    
      } catch (e) {
      }
    } else {
      throw new Error()
    }
  },
  async OtpApi (phone, otp) {
    let response = await makeRequest('/session', {
      method: 'PUT',
      body: JSON.stringify({phone, otp})
    })
    if ((response.status & 200) === 200 || response.status === 403) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async PersonalDataApi (name, email, referrer, from_where, income_type, income_range, married, kids, cibil, default_recent) {
    let response = await makeRequest('/user', {
      method: 'PUT',
      body: JSON.stringify({name, email, referrer, from_where, income_type, income_range, married, kids, cibil, default_recent})
    })
    if ((response.status & 200) === 200) {
      let content = await response.text()
      try {
        return JSON.parse(content)
      } catch (e) {
        return content
      }
    } else {
      throw new Error()
    }
  },
  async PanApi (pan, dob) {
    let response = await makeRequest('/documents', {
      method: 'POST',
      body: JSON.stringify({documentType: 'pan', value: pan, extra_data: {dob}})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async AddAadhaarApi (aadhaar, name, cookie, csrfToken, otp) {
    let response = await makeRequest('/documents', {
      method: 'POST',
      body: JSON.stringify({documentType: 'aadhaar', value: aadhaar, extra_data: {name}, csrfToken, otp, cookie: cookie.JSESSIONID})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async SetPasswordApi (password, confirmPassword) {
    let response = await makeRequest('/set_password', {
      method: 'PUT',
      body: JSON.stringify({password, confirmPassword})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async DoRechargeApi (number, operatorId, amount, special, password) {
    let response = await makeRequest('/recharges', {
      method: 'POST',
      body: JSON.stringify({service_number: number, operator_id: operatorId, special, password, amount})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async RechargeStatusApi (id) {
    let response = await makeRequest(`/recharges/${id}`)
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async GmailTokenApi (token) {
    let response = await makeRequest(`/documents/gmail`, {
      method: 'POST',
      body: JSON.stringify({token})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async UpdateFcmTokenApi (token) {
    let response = await makeRequest(`/device`, {
      method: 'POST',
      body: JSON.stringify({token, os: Platform.OS})
    })
    if ((response.status & 200) === 200) {
      return
    } else {
      throw new Error()
    }
  },
  async RepaymentApi (amount) {
    let response = await makeRequest(`/payments/repayment`, {
      method: 'POST',
      body: JSON.stringify({amount})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  }
}
