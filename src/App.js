import React, { Component } from 'react';
import ReactTooltip         from 'react-tooltip';
import {Animated}           from "react-animated-css";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link 
}                           from 'react-router-dom';

import keys from './keys';

import uuid from 'uuid/v1';

import './App.scss';

import Login from './screens/login';
// import Otp from './screens/otp';
import OnboardingScreen from './screens/onboarding';

// import Personaldata from './screens/personalData';
// import Pan from './screens/pan';
// import Aadhar from './screens/aadhar';
// import Bankstatement from './screens/bankStatment';
// import Password from './screens/password';
// import Success from './screens/success';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      data : {
        name : 'spiderman'
      }
    }
    // console.log(uuid());
    // keys.deviceId = new Date().getTime();
    // console.log(keys);
  }

  componentDidMount(){
    
  }

  componentWillMount(){

  }

  render() {
    return (
      <Router>
        <div className="App">
          <Link className="text-center" to={'/'}>HOME</Link>
          <div className="">
            <Switch>
              <Route exact path='/' component={Login} />
              <Route exact path='/onboarding' component={OnboardingScreen} />
            </Switch>
          </div>
        </div>
      </Router>
    )
  }
}

export default App;
