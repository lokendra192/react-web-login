// import {StatusBar} from 'react-native'

// import RNSecureKeyStore from 'react-native-secure-key-store'
// import DeviceInfo from 'react-native-device-info'

// import { Platform } from 'react-native'

// import Billing from './entities/Billing'
// import Category from './entities/Category'
// import Offer from './entities/Offer'
// import Onboarding from './entities/Onboarding'
// import Service from './entities/Service'
// import User from './entities/User'

import keys from './keys';
// import uuid from 'uuid/v1';

// import { getLastLocation } from './utils/LocationListener'

const JSON_HEADER = 'application/json'
// const API_BASE = __DEV__ ? 'http://192.168.1.181:3001' : 'https://api.galaxycard.in'
// const API_BASE = 'https://api.galaxycard.in';
// const API_BASE = "http://192.168.1.59:3001";
const API_BASE = "https://7f375ec1.ngrok.io";
// const API_BASE = ''

// let device = async()=>{return uuid()};

var makeRequest = async (url, options) => {
  options = options || {}
  if (!options.full_url) {
    url = `${API_BASE}/v${options.version || 1}${url}`
  }
  
  console.log('in makeRequest ' , keys.token , keys.deviceId);

  options.headers = options.headers || {}
  
  if(keys.token !== "") options.headers['x-auth-token'] = keys.token;
  if(keys.deviceId !== "") options.headers['device'] = keys.deviceId;
  // options.headers['device'] = device;

  // options.headers.device = 'ceeb71cb-45bf-4838-a94d-643b3d948ed9';
  // options.headers['x-auth-token'] = 'fe30d89d-5b56-47ca-8418-04219cf3465b';

  // try {
  //   options.headers['x-auth-token'] = keys.token;
  // } catch (e) {}
  // try {
  //   options.headers.device = keys.deviceId;
  // } catch (e) {}

  // options.headers.platform = "android";
  // try {
  // options.headers['x-auth-token'] = options.token;
  // } catch (e) {}
  // try {
  // options.headers.device = options.device;
  // } catch (e) {}

  // console.log('device ' + options.device);
  // console.log( 'x auth ' +  options.token )

  // try {
  //   options.headers.device = await RNSecureKeyStore.get('device')
  // } catch (e) {}

  // options.headers.platform = 'web'

  options.headers['Content-Type'] = options.headers['Content-Type'] || JSON_HEADER

  let opts = {
    headers: options.headers,
    method: options.method || 'GET'
  }
  
  if (options.body) {
    opts.body = options.body
  }

  // if (Platform.OS === 'ios') {
  //   StatusBar.setNetworkActivityIndicatorVisible(true)
  // }

  try {
    let response = await fetch(url, opts)
    if (response.status === 401) {
      throw new Error('logout')
    } else if (response.status === 426) {
      throw new Error('upgrade')
    }
    // if (Platform.OS === 'ios') {
    //   StatusBar.setNetworkActivityIndicatorVisible(false)
    // }
    return response
  } catch (e) {
    // if (Platform.OS === 'ios') {
    //   StatusBar.setNetworkActivityIndicatorVisible(false)
    // }
    throw e
  }

}
// export default makeRequest;
// export default = SyncApi = function(){
//   let response = await makeRequest('/')
//   if ((response.status & 200) === 200) {
//     response = await response.json()
//     await Promise.all([
//       Offer.saveAll(response.offers),
//       Service.saveAll(response.services),
//       Onboarding.saveAll(response.onboarding),
//       User.saveAll(response.user),
//       Billing.saveAll(response.billing),
//       Category.saveAll(response.categories),
//       // RNSecureKeyStore.set('application_status', response.status)
//     ])
//   } else {
//     throw new Error()
//   }
// }

module.exports = {
  async SyncApi () {
    // console.log('token in SyncApi ', token);
    let response = await makeRequest('/')
    // let data = response.then(e=>e);
    // console.log(data);
    // if ((response.status & 200) === 200) {
    //   response = await response.json()
    //   console.log(response);
    //   // await Promise.all([
    //     // Offer.saveAll(response.offers),
    //     // Service.saveAll(response.services),
    //     // Onboarding.saveAll(response.onboarding),
    //     // User.saveAll(response.user),
    //     // Billing.saveAll(response.billing),
    //     // Category.saveAll(response.categories),
    //     // RNSecureKeyStore.set('application_status', response.status)
    //   // ])
    //   // return response;
    // } else {
    //   throw new Error()
    // }
  },
  async SyncApi1 () {
    // console.log('token in SyncApi ', token);
    let response = await makeRequest('/')
    // let data = response.then(e=>e);
    // console.log(data);
    if ((response.status & 200) === 200) {
      response = await response.json()
      // console.log(response);
      // await Promise.all([
        // Offer.saveAll(response.offers),
        // Service.saveAll(response.services),
        // Onboarding.saveAll(response.onboarding),
        // User.saveAll(response.user),
        // Billing.saveAll(response.billing),
        // Category.saveAll(response.categories),
        // RNSecureKeyStore.set('application_status', response.status)
      // ])
      return response;
    } else {
      throw new Error()
    }
  },
  async LoginApi (phone) {
    console.log('in login api');
    let response = await makeRequest('/session', {
      method: 'POST',
      body: JSON.stringify({phone})
    })
    if ((response.status & 200) === 200) {
      try {
        return await response.json()
      } catch (e) {
      }
    } else {
      throw new Error()
    }
  },
  async OtpApi (phone, otp) {
    let response = await makeRequest('/session', {
      method: 'PUT',
      body: JSON.stringify({phone, otp})
    })
    if ((response.status & 200) === 200 || response.status === 403) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async PersonalDataApi (name, email, referrer, from_where, income_type, income_range, married, kids, cibil, default_recent) {
    let response = await makeRequest('/user', {
      method: 'PUT',
      body: JSON.stringify({name, email, referrer, from_where, income_type, income_range, married, kids, cibil, default_recent})
    })
    if ((response.status & 200) === 200) {
      let content = await response.text()
      try {
        return JSON.parse(content)
      } catch (e) {
        return content
      }
    } else {
      throw new Error()
    }
  },
  async PanApi (pan, dob) {
    let response = await makeRequest('/documents', {
      method: 'POST',
      body: JSON.stringify({documentType: 'pan', value: pan, extra_data: {dob}})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async AddAadhaarApi (aadhaar, name, cookie, csrfToken, otp) {
    let response = await makeRequest('/documents', {
      method: 'POST',
      body: JSON.stringify({documentType: 'aadhaar', value: aadhaar, extra_data: {name}, csrfToken, otp, cookie: cookie})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async SetPasswordApi (password, confirmPassword) {
    let response = await makeRequest('/set_password', {
      method: 'PUT',
      body: JSON.stringify({password, confirmPassword})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async DoRechargeApi (number, operatorId, amount, special, password) {
    let response = await makeRequest('/recharges', {
      method: 'POST',
      body: JSON.stringify({service_number: number, operator_id: operatorId, special, password, amount})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async RechargeStatusApi (id) {
    let response = await makeRequest(`/recharges/${id}`)
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async AddItrApi (itr) {
    let response = await makeRequest(`/documents`, {
      method: 'POST',
      body: JSON.stringify({documentType: 'itr', itr})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async GmailTokenApi (token) {
    // console.warn(token)
    let response = await makeRequest(`/documents/gmail`, {
      method: 'POST',
      body: JSON.stringify({token})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async UpdateFcmTokenApi (token) {
    let response = await makeRequest(`/device`, {
      method: 'POST',
      body: JSON.stringify({token, os: Platform.OS})
    })
    if ((response.status & 200) === 200) {
      return
    } else {
      throw new Error()
    }
  },
  async RepaymentApi (amount) {
    let response = await makeRequest(`/payments/repayment`, {
      method: 'POST',
      body: JSON.stringify({amount})
    })
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async AadharToken () {
    let response = await makeRequest('/documents/uidai_init');
    if ((response.status & 200) === 200) {
      return await response.json()
    } else {
      throw new Error()
    }
  },
  async SendAadharToken (cookie, csrfToken) {
    let response = await makeRequest('/documents/uidai_captcha' , {
      method: 'POST',
      body: JSON.stringify({cookie , csrfToken})
    })
    if ((response.status & 200) === 200) {
      return await response.text()
    } else {
      throw new Error()
    }
  },
  async SendAadharDetails (aadhaar, name , pincode , captcha, cookie , csrfToken) {
    let response = await makeRequest('/documents/uidai_otp' , {
      method: 'POST',
      body: JSON.stringify({aadhaar , name , pincode , captcha, cookie , csrfToken})
    })
    if ((response.status & 200) === 200) {
      return response.status;
      // return await response.json()
    } else {
      throw new Error()
    }
  },
  async iconUrl (klass, ids) {
    let headers = {}
    let token = RNSecureKeyStore.get('token')
    let device = RNSecureKeyStore.get('device')
    let battery = DeviceInfo.getBatteryLevel()
    try {
      headers['x-auth-token'] = await token
    } catch (e) {}
    try {
      headers.device = await device
    } catch (e) {}
    headers.platform = Platform.OS === 'ios' ? 'ios' : 'android'
    headers.version = '' + DeviceInfo.getBuildNumber()
    headers.battery = '' + await battery
    headers.brand = DeviceInfo.getBrand()
    headers.model = DeviceInfo.getModel()
    if (ids instanceof Array) {
      let result = {}
      for (let i = 0, len = ids.length; i < len; i++) {
        result[ids[i]] = {
          uri: `${API_BASE}/v1/images/${klass.toLowerCase()}/${ids[i]}/full`,
          method: 'GET',
          headers
        }
      }
      return result
    } else {
      return {
        uri: `${API_BASE}/v1/images/${klass.toLowerCase()}/${ids}/full`,
        method: 'GET',
        headers
      }
    }
  }
}
