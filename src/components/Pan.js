import React, { Component } from 'react'

export class Pan extends Component {
  constructor(props) {
    super(props)
    // console.log(this.props);
    // this.state = {
    //   pan: null,
    //   dob: null,
    //   day: null,
    //   month: null,
    //   year: null,
    //   loading: false,
    //   pickerVisible: false
    // }
    this.state = {
      pan: 'CNFPS6109G',
      dob: '26/07/1997',
      day: null,
      month: null,
      year: null,
      loading: false,
      pickerVisible: false
    }
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    return {
      loading: nextProps.loading
    }
  }

  _createDateData() {
    let date = []
    let currentYear = new Date().getFullYear()
    for (let i = currentYear - 100; i < currentYear; i++) {
      let month = []
      for (let j = 1; j < 13; j++) {
        let day = []
        if (j === 2) {
          for(let k = 1; k < 29; k++){
            day.push('' + k)
          }
          //Leap day for years that are divisible by 4, such as 2000, 2004
          if (i % 100 === 0) {
            if (i % 4 === 0) {
              day.push('' + 29)
            }
          } else if (i % 4 === 0) {
            day.push('' + 29)
          }
        }
        else if ([1, 3, 5, 7, 8, 10, 12].includes(j)) {
          for (let k = 1; k < 32; k++) {
            day.push('' + k)
          }
        } else {
          for (let k = 1; k < 31; k++) {
            day.push('' + k)
          }
        }
        let _month = {}
        _month[j] = day
        month.push(_month)
      }
      let _date = {}
      _date[i] = month
      date.push(_date)
    }
    return date
  }

  onSubmit () {
    // this.setState({dob : this.state.dob.split("-").reverse().join('/')})
    if (!/\d{2}\/\d{2}\/\d{4}/.test(this.state.dob)) {
      alert('Error', 'Please enter your date of birth in DD/MM/YYYY format. For example, if your date of birth is 2nd May, 1987 then enter, 02/05/1987', [
        {text: 'OK'}
      ], {
        cancelable: false
      })
    } else {
      this.props.onDone({
        pan: this.state.pan,
        dob: this.state.dob
      })
    }
  }

  render() {
    return (
      <div className="border p-4 m-4">
        <h1>Your PAN number</h1>
        <input type="text" placeholder="Your PAN Number" onChange={(e)=>{
          let pan = e.target.value;
          pan = (pan || '').trim();
          this.setState({pan})
        }} />
        <p> {this.state.pan} </p>
        <input onChange={e=>{this.setState( {dob : e.target.value.split("-").reverse().join('/')} )}} type="date" name="bday" />
        <p>{this.state.dob}</p>
        <button className="btn btn-lg btn-primary " onClick={() => this.onSubmit()}>
          NEXT
        </button>
      </div>
    )
  }
}
