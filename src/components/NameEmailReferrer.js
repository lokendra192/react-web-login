import React, { Component } from 'react'

// import {
//   RkText,
//   RkTextInput
// } from 'react-native-ui-kitten'

// import {
//   View,
//   SafeAreaView,
//   StyleSheet,
//   Keyboard
// } from 'react-native'

import {GradientButton} from './gradientButton';
// import {scale, scaleVertical} from '../utils/scale';

export class NameEmailReferrer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: null,
      email: null,
      referrer: null,
      from_where: null
    }
  }

  // static getDerivedStateFromProps (nextProps, prevState) {
  //   return {
  //     name: nextProps.name,
  //     email: nextProps.email,
  //     referrer: nextProps.referrer,
  //     from_where: nextProps.from_where
  //   }
  // }

  onSubmit () {
    console.log('onSubmit from NameEmailReferrer ran')
    if (this.state.name && this.state.email) {
      this.props.onChange({
        name: this.state.name,
        email: this.state.email,
        referrer: this.state.referrer,
        from_where: this.state.from_where
      })
    }
  }

  render() {
    return (
      <div>
        <div>
          <h2>Let's Get Started</h2>
          <input className="form-control" type="text" placeholder='Your Name' onChange={(e) => { this.setState({name : e.target.value}) }}/>
          <input className="form-control" type="email" placeholder='Your Email' onChange={(e) => {
            let email = e.target.value;
            email = (email||'').trim()
            this.setState({email})
          }}/>
          <input className="form-control" placeholder='Your Referrer (Optional)' onChange={(e) => {
            let referrer = e.target.value;
            referrer = (referrer||'').trim()
            this.setState({referrer})
          }}/>
          <input className="form-control" placeholder='How did you hear about us?' onChange={(e) => {
            let from_where = e.target.value;
            from_where = (from_where||'').trim()
            this.setState({from_where: from_where})
          }}/>
          <button className="btn btn-primary" disabled={!this.state.name || !this.state.email || (this.state.name || '').trim().length < 2 || (this.state.email || '').trim().length < 4 || (this.state.from_where || '').trim().length < 2} onClick={() => this.onSubmit()} >
            next
          </button>
        </div>
      </div>
    )
  }
}
