import React, { Component } from 'react'

import cheerio from 'cheerio-without-node-native'

import {AadharToken , SendAadharToken , SendAadharDetails }  from '../apis';

// var CookieManager = require('react-native-cookies');

export class Aadhaar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: props.loading,
      aadhaar: "542514956020",
      pincode: "110022",
      name: 'Amit Kumar',
      cookie : null,
      csrfToken: null,
      otpSent: false,
      captchaImage: null,
      captcha: null,
      otp: null
    }

    this.onSubmit = this.onSubmit.bind(this);
    this.downloadFile = this.downloadFile.bind(this);
  }

  //otp 020260

  // static getDerivedStateFromProps (nextProps, prevState) {
  //   return {
  //     loading: nextProps.loading
  //   }
  // }

  async componentDidMount() {
    // this.fetchUidaiData()
    var data = await AadharToken();
    // console.log(data.cookies.JSESSIONID , data.csrfToken);
    this.setState({cookie : data.cookies.JSESSIONID , csrfToken : data.csrfToken})
    var captcha = SendAadharToken(this.state.cookie , this.state.csrfToken);
    captcha.then(res=>{
      console.log(res)
      this.setState({captchaImage : res})
      // console.log(this.state);
    })
    // console.log(this.state  , captcha);
  }

  // async fetchUidaiData () {
  //   let response = await fetch('https://eaadhaar.uidai.gov.in/#/', {
  //     credentials: 'same-origin'
  //   })

  //   // CookieManager.get('http://eaadhaar.uidai.gov.in')
  //   // .then((res) => {
  //   //   this.setState({cookie: res})
  //   // })

  //   response = await response.text()
  //   const $ = cheerio.load(response)
  //   this.setState({csrfToken: $('#CSRFToken').val()})
  //   this.getCaptchaImage()
  // }

  // async getCaptchaImage () {
  //   this.setState({captchaImage: null})
  //   let data = await fetch('https://eaadhaar.uidai.gov.in/captcha-image.html', {
  //     method: 'POST',
  //     body: `CSRFToken=${this.state.csrfToken}`,
  //     headers: {
  //       'Content-Type': 'application/x-www-form-urlencoded',
  //       Referer: 'https://eaadhaar.uidai.gov.in/',
  //       'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
  //       'X-Requested-With': 'XMLHttpRequest'
  //     }
  //   })
  //   this.setState({captchaImage: `data:image/jpeg;base64,${await data.text()}`})
  // }

  async sendOtp() {
    this.setState({loading: true})
    try {
      let response = await fetch('https://eaadhaar.uidai.gov.in/genOTPByValidatingDetails.html', {
        method: 'POST',
        credentials: 'same-origin',
        body: `EID=${this.state.aadhaar.substr(0, 4)}/${this.state.aadhaar.substr(4, 4)}/${this.state.aadhaar.substr(8, 4)}&DATETIME=&NAME=${(this.state.name || '').trim()}&PINCODE=${this.state.pincode}&MOBILE=&CAPTCHA=${this.state.captcha}&RESENDOTP=false&CSRFToken=${this.state.csrfToken}`,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Referer: 'https://eaadhaar.uidai.gov.in/',
          'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
          'X-Requested-With': 'XMLHttpRequest'
        }
      })
      if (response.status === 200) {
        response = await response.text()
        if (response.includes('success')) {
          response = await fetch('https://eaadhaar.uidai.gov.in/sendOTPtoRegisteredMobile.html', {
            credentials: 'same-origin',
            method: 'POST',
            body: `RESENDOTP=false&CSRFToken=${this.state.csrfToken}`,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              Referer: 'https://eaadhaar.uidai.gov.in/',
              'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
              'X-Requested-With': 'XMLHttpRequest'
            }
          })
          if (response.status === 200) {
            response = await response.text()
            if (response.includes('success')) {
              Alert.alert('OTP Sent', 'OTP has been sent by UIDAI to your Aadhaar linked mobile number', [
                {text: 'OK', onPress: () => this.setState({otpSent: true})}
              ])
            } else {
              throw new Error()
            }
          } else {
            throw new Error()
          }
        } else {
          throw new Error()
        }
      } else {
        throw new Error()
      }
    } catch (e) {
      this.fetchUidaiData()
      Alert.alert('Error', 'An error occured trying to generate an OTP. Please try again.', [
        {text: 'OK'}
      ])
    }
    this.setState({loading: false})
  }

  async downloadFile () {
    try {
      await this.props.onSubmit(this.state.aadhaar, this.state.name, this.state.cookie , this.state.csrfToken, this.state.otp)
    } catch (e) {
      alert('Error an error occured trying to fetch your Aadhaar details. Please try again.');
      this.setState({otpSent: false})
    }
  }

  getCaptchaSource () {
    if (this.state.captchaImage) {
      return {
        uri: this.state.captchaImage
      }
    }
  }

  async onSubmit(){
    var res = SendAadharDetails(this.state.aadhaar , this.state.name , this.state.pincode , this.state.captcha , this.state.cookie, this.state.csrfToken)
    // console.log(res);
    res.then(response=>{
      if(response == 200){
        this.setState({otpSent : true })
      }
    })
    // this.setState({otpSent : true});
  }

  render() {
    // if (!this.state.csrfToken) {
      // var view;
      // if(this.state.otpSent){
      //   view = <input type="number" placeholder='OTP' onChange={(e) => {let otp = e.target.value;otp = (otp||'').trim() this.setState({otp})}}/>
      // }
      return (
        <div className="border m-4 p-4">
          <h1>Your Aadhaar Card Details</h1>

            {!this.state.otpSent && <input type="number" placeholder='Your Aadhaar Number' onChange={(e) => {
              let aadhaar = e.target.value;
              aadhaar = (aadhaar||'').trim()
              this.setState({aadhaar})
            }}/>}

            {!this.state.otpSent && <input type="number"  ref={(ref) => { this.pincodeRef = ref }} placeholder='Pincode as on Aadhaar'onChange={(e) => {
              let pincode = e.target.value;
              pincode = (pincode||'').trim()
              this.setState({pincode})
            }} />}

            {!this.state.otpSent && <input type="text" placeholder='Full Name as on Aadhaar' onChange={(e) => {
              this.setState({name : e.target.value})
            }} />}

            {!this.state.otpSent && <div>
              <img src={this.state.captchaImage} /><br/>
              <input type="number" value={this.state.captcha} placeholder='Captcha' onChange={(e) => {
                let captcha = e.target.value
                captcha = (captcha||'').trim()
                this.setState({captcha})
              }} />
            </div>}

            {this.state.otpSent && <div>
              <p>{this.state.otp}</p>
              <input className="form-control" type="number" placeholder='OTP' 
                onChange={(e) => {
                  let otp = e.target.value
                  otp = (otp||'').trim()
                  this.setState({otp})
                }}
              /><button className= "btn btn-primary" onClick={() => this.downloadFile()} >
             Submit Otppp
            </button></div>
            }

            {this.state.otpSent && <input className="d-none" type="number" placeholder='OTP' onChange={(e) => {
              let otp = e.target.value
              otp = (otp||'').trim()
              this.setState({otp})
            }} />}

            {!this.state.otpSent && <button className="btn btn-primary btn-lg" onClick={() => this.onSubmit()} >
             get otp
            </button>}

            <br/>
            {this.state.otpSent && <button className="btn btn-primary" onClick={()=>{this.setState({otpSent : false})}} > go back </button> }

            <button className= "d-none btn btn-primary btn-lg" onClick={() => this.downloadFile()} >
             Next
            </button>

          <button className="d-none btn btn-primary btn-lg" disabled={
            this.state.loading || (this.setState.otpSent ? (this.state.otp || '').trim().length !== 6 : ((this.state.name || '').trim().length < 4 || (this.state.pincode || '').trim().length < 6 || (this.state.aadhaar || '').trim().length < 12))} onClick={() => this.state.otpSent ? this.downloadFile() : this.sendOtp()} >
            {this.state.loading ? 'Please Wait...' : (this.state.otpSent ? 'Next' : 'Get OTP')} 
          </button>
        </div>
      )
    // } else {
    //   return (
    //     <div>
    //       <h1> loading... </h1>
    //     </div>
    //   )
    // }
  }
}
