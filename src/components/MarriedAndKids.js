import React, { Component } from 'react'

// import {
//   RkChoiceGroup,
//   RkChoice,
//   RkText
// } from 'react-native-ui-kitten'

// import {
//   View,
//   TouchableOpacity,
//   StyleSheet
// } from 'react-native'

// import {GradientButton} from './gradientButton';
// import {scale, scaleVertical} from '../utils/scale';

export class MarriedAndKids extends Component {
  constructor(props) {
    super(props)
    this.state = {
      married: null,
      kids: null
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return nextProps.married !== this.props.married || nextProps.kids !== this.props.kids
  // }

  // static getDerivedStateFromProps (nextProps, prevState) {
  //   return {
  //     married: nextProps.married,
  //     kids: nextProps.kids
  //   }
  // }

  render() {
    return (
      <div>
        <h3>Personal Details</h3>
        <div>
          <h4>Please select your marital status</h4>
        </div>
        <div>
          <label >
            married 
            <input onChange={()=>{this.setState({married :  1})}} type="radio" value="1" name="martial_status"  />
          </label>
          <label > 
            not married 
            <input onChange={()=>{this.setState ({married : 0})}} type="radio" value="0" name="martial_status" />
          </label>
        </div>

        <div>
          <h4>How many children do you have</h4>
        </div>
        <div>

          <label >
            none 
            <input onChange={()=>{this.setState({kids : 'none'})}} type="radio" value="1" name="kids"  />
          </label>

          <label >
            1-2 
            <input onChange={()=>{this.setState({kids : '1-2'})}} type="radio" value="1" name="kids"  />
          </label>

          <label >
            3+
            <input onChange={()=>{this.setState({kids : '3+' })}} type="radio" value="1" name="kids"  />
          </label>

        </div>
        <button className="btn btn-primary" onClick={() => {
          this.props.onChange({
            married: this.state.married,
            kids: this.state.kids
          })
        }}> next </button>

      </div>
    )
  }
}