import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  VirtualizedList,
  TouchableOpacity
} from 'react-native';

import {
  RkCard, RkText
} from 'react-native-ui-kitten'

import { scaleVertical, scale } from '../utils/scale';

export default class ServicesView extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.services.length !== this.props.services.length) {
      return true
    }
    for (let i = 0, len = nextProps.services.length; i < len; i++) {
      if (nextProps.services[i].id !== this.props.services[i]) {
        return true
      }
    }
    return false
  }

  _renderItem (item) {
    let image = null
    switch (item.code) {
      case 'outlet':
        image = require('../assets/images/outlet.webp')
        break
      case 'mobile':
        image = require('../assets/images/mobile.webp')
        break
      case 'dth':
        image = require('../assets/images/dth.webp')
        break
      case 'gas':
        image = require('../assets/images/gas.webp')
        break
      case 'broadband':
        image = require('../assets/images/broadband.webp')
        break
      case 'datacard':
        image = require('../assets/images/datacard.webp')
        break
      case 'electricity':
        image = require('../assets/images/electricity.webp')
        break
      case 'landline':
        image = require('../assets/images/landline.webp')
        break
      default:
        return
    }
    return (<TouchableOpacity onPress={() => {
      this.props.onServiceSelected(item.id, item.code, item.name)
    }} delayPressIn={35} activeOpacity={0.8}>
      <View style={styles.service}>
        <Image source={image} style={styles.serviceIcon} />
        <RkText rkType='header6'>{item.name}</RkText>
      </View>
    </TouchableOpacity>)
  }

  render() {
    return (
      <RkCard rkType='homeCard' style={[this.props.style]}>
        <View rkCardContent>
          <VirtualizedList data={this.props.services}
            keyExtractor={(item) => ''+item.id}
            getItemCount={() => this.props.services ? this.props.services.length : 0}
            getItem={(data, index) => data[index]}
            horizontal={true}
            ItemSeparatorComponent={() => <View style={{marginRight: scale(16)}}/>}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            directionalLockEnabled
            renderItem={({item}) => this._renderItem(item)} />
        </View>
      </RkCard>
    )
  }
}

let styles = StyleSheet.create({
  service: {
    alignItems: 'center'
  },
  serviceIcon: {
    width: scale(48),
    height: scaleVertical(48)
  }
})