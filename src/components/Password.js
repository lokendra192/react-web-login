import React, { Component } from 'react'

// import CodeInput from 'react-native-confirmation-code-input'

// import {scale, scaleVertical} from '../utils/scale'

// import { KittenTheme } from '../config/theme'

// import {GradientButton} from './gradientButton'

export class Password extends Component {
  constructor(props) {
    super(props)
    this.state = {
      code: null,
      confirmCode: null,
      loading: false
    }
  }

  async onSubmit () {
    if (this.state.code === null || this.state.code.trim().length < 4) {
      alert('Error Please enter a valid PIN')
      this.setState({code: null, confirmCode: null})
    } else if (this.state.code !== this.state.confirmCode) {
      alert('Error Your PINs do not match')
      this.setState({code: null, confirmCode: null})
    } else {
      this.setState({loading: true})
      try {
        await this.props.onSubmit(this.state.code.trim(), this.state.confirmCode.trim())
      } catch (e) {
        alert('Error Your PIN could not be saved')
        this.setState({code: null, confirmCode: null})
      }
      this.setState({loading: false})
    }
  }

  render() {
    return (
      <div className="border m-4 p-4">
        <h1> Add Password </h1>
        <h4>Secure Your Account</h4>
        <div>
          <input onChange= {(e)=> {this.setState({code : e.target.value})}} type="password" placeholder="your pin"  /> 
          <input onChange= {(e)=> {this.setState({confirmCode : e.target.value})}} type="password" placeholder="confirm your pin"  />
        </div>
        <br/>
        <button className="btn btn-primary btn-lg" onClick={() => this.onSubmit()} >
          next
        </button>
      </div>
    )
  }
}
