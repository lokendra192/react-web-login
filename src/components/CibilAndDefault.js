import React, { Component } from 'react'

// import {
//   RkChoiceGroup,
//   RkChoice,
//   RkText,
//   RkTextInput,
//   RkAvoidKeyboard
// } from 'react-native-ui-kitten'

// import {
//   View,
//   TouchableOpacity,
//   StyleSheet,
//   Keyboard
// } from 'react-native'

// import {GradientButton} from '../components/gradientButton';
// import {scale, scaleVertical} from '../utils/scale';

export class CibilAndDefault extends Component {
  constructor(props) {
    super(props)
    this.state = {
      cibil: null,
      default_recent: null
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return nextProps.cibil !== this.props.cibil || nextProps.default_recent !== this.props.default_recent || nextState.cibil !== this.state.cibil || nextState.default_recent !== this.state.default_recent
  // }

  // static getDerivedStateFromProps (nextProps, prevState) {
  //   return {
  //     loading: nextProps.loading,
  //     cibil: nextProps.cibil,
  //     default_recent: nextProps.default_recent
  //   }
  // }

  render() {
    return (
      <div className="border p-4 m-4">
        <h3>Credit History</h3>
        <h4>Do you know your CIBIL score?</h4>
        <div>
          <label>
            yes
            <input onChange={()=>{this.setState({cibil : true })}} type="radio" value="true" name="cibil"  />
          </label>
          <label >
            no
            <input onChange={()=>{this.setState({cibil : false})}} type="radio" value="false" name="cibil"  />
          </label>
        </div>

        {console.log(this.state.cibil)}

        {!!this.state.cibil && <input placeholder='Your CIBIL Score' type="number" onChange={(e) => {
          this.setState({cibil : e.target.value})
        }}/>}

        <h4>Have you defaulted on an EMI payment in the last 3 years?</h4>

        <div>
           <label >
              yes
              <input onChange={()=>{this.setState({default_recent : true })}} type="radio" value="true" name="default_recent"  />
            </label>
            <label >
              no
              <input onChange={()=>{this.setState({default_recent : false})}} type="radio" value="false" name="default_recent"  />
          </label>
        </div>

        <button
          className = "btn btn-primary"
          onClick={() => {
            this.props.onChange({
              cibil: this.state.cibil,
              default_recent: this.state.default_recent
            })
          }}
         > next 
         </button>
      </div>
    )
  }
}
