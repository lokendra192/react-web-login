import React, { Component } from 'react'

// import {
//   RkChoiceGroup,
//   RkChoice,
//   RkText
// } from 'react-native-ui-kitten'

// import {
//   View,
//   TouchableOpacity,
//   StyleSheet
// } from 'react-native'

// import {scale, scaleVertical} from '../utils/scale';

export class IncomeType extends Component {
  
  // shouldComponentUpdate(nextProps, nextState) {
  //   return nextProps.income_type !== this.props.income_type
  // }

  render() {
    return (
      <div>
        <div>
          <h1>You Are</h1>
          <div>
            <label>
              <input onChange={()=>{this.props.onChange({income_type : 'salaried'})}} type="radio" value="salaried" name="income_type"  />
              salaried
            </label>
            <label>
              <input onChange={()=>{this.props.onChange({income_type : 'self-employed'})}} type="radio" value="self-employed" name="income_type" />
              self-employed
            </label>
          </div>
        </div>
      </div>
    )
  }
}