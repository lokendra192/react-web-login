import React, {Component} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

import {
  RkCard, RkButton, RkText
} from 'react-native-ui-kitten'

import {
  KittenTheme
} from '../config/theme'
import { scaleVertical } from '../utils/scale';

import {INR_FORMAT_WITHOUT_SYMBOL} from '../utils/textUtils'

let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

export default class BillingView extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.balance !== this.props.balance || nextProps.current_limit !== this.props.current_limit || nextProps.carried_over !== this.props.carried_over || nextProps.last_pay_by !== this.props.last_pay_by || nextProps.pay_by !== this.props.pay_by || nextProps.ends_at !== this.props.ends_at
  }

  timeTo (ends_at) {
    if (ends_at === undefined) {
      return ''
    }
    let days = Math.floor((ends_at - (new Date() / 1000)) / 86400)
    if (days < 0) {
      return `since ${days} day${(0 - days > 1 ? 's' : '')}`
    } else if (days === 0) {
      return 'today'
    } else {
      return `in ${days} day${days > 1 ? 's' : ''}`
    }
  }

  render() {
    return (
      <RkCard rkType='homeCard'>
        <View rkCardContent>
          <View style={styles.balance_container}>
            <RkText style={styles.balance_rupee_symbol} rkType='header2'>₹</RkText>
            <RkText style={styles.balance_amount} rkType='header0'>{this.props.balance === undefined ? '' : INR_FORMAT_WITHOUT_SYMBOL.format(this.props.balance)}</RkText>
          </View>
          <RkText style={styles.balance_helper_text} rkType='secondary1'>Available Balance</RkText>
          <View style={[styles.row, styles.payment]}>
            <RkText style={styles.previous_amount}>{this.props.carried_over <= 0 ? 'Cycle ends ' + this.timeTo(this.props.ends_at) : '₹' + INR_FORMAT_WITHOUT_SYMBOL.format(this.props.carried_over) + ' due ' + this.timeTo(this.props.last_pay_by)}</RkText>
            <RkButton rkType='clear' disabled={this.props.current_limit - this.props.balance <= 0 && this.props.carried_over <= 0} contentStyle={this.props.carried_over <= 0 ? styles.pay_disabled : styles.pay_enabled} onPress={() => this.props.onPayNow()}>Pay Now</RkButton>
          </View>
        </View>
      </RkCard>
    )
  }
}

let styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  payment: {
    marginTop: scaleVertical(16)
  },
  balance_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'baseline'
  },
  balance_rupee_symbol: {
    color: KittenTheme.colors.text.brand
  },
  balance_amount: {
    color: KittenTheme.colors.text.brand
  },
  balance_helper_text: {
    alignSelf: 'center'
  },
  previous_amount: {
    flex: 1,
    color: KittenTheme.colors.text.accent
  },
  pay_enabled: {
    color: KittenTheme.colors.button.highlight
  },
  pay_disabled: {
    color: KittenTheme.colors.button.disabled
  }
})