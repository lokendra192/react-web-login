import React, { Component } from 'react'

// import {GoogleSignin} from 'react-native-google-signin'
// import {ProgressBar} from '../components/progressBar'
// import {GradientButton} from './gradientButton';
// import {scale, scaleVertical} from '../utils/scale';

import GoogleLogin from 'react-google-login';

export class BankStatement extends Component {
  constructor(props) {
    super(props)
    // console.log(this.props);
    this.state = {
      token: null,
      statements_found: null,
      progress: 0,
      count : 0,
      error : false,
      loading: false
    }
    this.success = this.success.bind(this);
  }

  async componentDidMount () {
    // await GoogleSignin.configure({
    //   scopes: ['https://www.googleapis.com/auth/gmail.readonly'],
    //   iosClientId: '66778420592-lqdu5u6mpa92tocsclc1mhq7t1reb907.apps.googleusercontent.com',
    //   forceConsentPrompt: true
    // })
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.statements_found !== nextProps.statements_found) {
      if (nextProps.statements_found > 0) {
        // this.progressTimer = setInterval(() => {
        //   if (this.state.progress === 1) {
        //     clearInterval(this.progressTimer)
        //   } else {
        //     let progress = this.state.progress + (1 / 1200)
        //     if (progress > 1) {
        //       progress = 1
        //     }
        //     // this.setState({progress})
        //   }
        // }, 100)
        
      }
    }
    if (nextProps.statements_found === 0) {
      // if(this.state.count === 0){
      alert('No Bank Statements Found Uh-oh! We couldn\'t find any Bank Statements in your GMail account.');
      //   this.setState({count : 1})
      //   console.log(this.state.count);
      // }
      // this.setState({error : true})
      // if(this.state.count == 0){
        // alert(this.props.loading);
        // this.setState({count : 1})
      // }
    }
    return true
  }

  // limit(){
  //   this.setState({count : 1});
  // }

  static getDerivedStateFromProps (nextProps, prevState) {
    return {
      loading: nextProps.loading,
      statements_found: nextProps.statements_found
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer)
    }
  }

  setTimer() {
    if (this.timer) {
      clearInterval(this.timer)
    }
    this.timer = setInterval(() => {
      this.props.onTimer()
    }, 10000)
  }

  async testGooglePlayServices () {
    try {
      await GoogleSignin.hasPlayServices({ autoResolve: true })
    } catch (e) {
      alert('Google Play Services error. An error occurred while trying to connect with Google Play Services. Please check if they are installed on your device, and are the updated to the latest version.', [
        {text: 'cancel', style: 'cancel'},
        {text: 'OK', onPress: () => this.testGooglePlayServices()}
      ], {
        cancelable: false
      })
    }
  }

  async onLoginToGmail () {
    this.setState({loading: true})
    try {
      // await this.testGooglePlayServices()
      // if (Platform.OS === 'android') {
      //   let accessToken = await GoogleSignin.getAccessToken()
      //   await this.props.onSubmitToken(user.accessToken)
      // } else {
      let {accessToken} = await GoogleSignin.signIn()
      await this.props.onSubmitToken(accessToken)
      // }
      // this.setTimer()
    } catch (e) {
      console.error(e)
    }
    this.setState({loading: false})
  }

  timeElapsed () {
    let time = Math.round(120 - (this.state.progress * 120))
    if (time < 60) {
      return `${time} seconds to go`
    } else {
      time = time - 60
      return `1 minute and ${time < 10 ? '0' + time : time} seconds to go`
    }
  }

  renderButtons () {
    if (this.state.loading) {
      return (
        <p>Loading... </p>
      )
    } else {
      // let {width} = Dimensions.get('window')
      return (
        <div>
          {this.state.statements_found === null && <button className="btn btn-primary btn-lg" onClick={() => this.onLoginToGmail()}> Fetch Bank Statements from Gmail </button>}
          {this.state.statements_found > 0 && 
            <div>
              {this.state.progress < 1 && <p> please wait </p>}
              {this.state.progress === 1 && <div>loading...</div>}
              <p>{this.state.progress < 1 ? this.timeElapsed() : 'This seems to be taking longer than usual. Please wait...'}</p>
            </div>
          }
        </div>
      )
    }
  }

  async success(obj){
    console.log('in success ' ,obj);
    // alert('success');
    await this.props.onSubmitToken(obj.accessToken);
    // if(accessToken){
    //   await this.props.onSubmitToken(accessToken);
    // }
    // this.setTimer();
  }

  fail(){
    alert('fail');
  }

  render() {
    //{this.renderButtons()}
    return (
      <div className="border m-4 p-4">
        <h1> Bank Statements </h1>
        <div>
          <div>
            <p className="lead text-secondary">{this.state.statements_found > 0 ? 'Processing your Bank Statements' : 'Your Bank Statements helps us decide your credit limit'}
            </p>
              <GoogleLogin
                scope ="https://www.googleapis.com/auth/gmail.readonly"
                clientId="66778420592-rdu5afmutoj22mt1pf8ju5hscsatdjdo.apps.googleusercontent.com"
                buttonText="Fetch Bank Statements from Gmail"
                onSuccess={this.success}
                onFailure={this.fail}
              />
          </div>
        </div>
        {this.state.error && <p className="text-danger">No Bank Statements Found</p>}
      </div>
    )
  }

}
