import React, { Component } from 'react'

// import {
//   RkText
// } from 'react-native-ui-kitten'

// import {
//   KittenTheme
// } from '../config/theme'

// import {
//   View,
//   TouchableOpacity,
//   StyleSheet,
//   Slider
// } from 'react-native'

// import {scale, scaleVertical} from '../utils/scale'

// import {INR_FORMAT_WITHOUT_SYMBOL} from '../utils/textUtils'

// import {GradientButton} from './gradientButton'

const MAX = 120

export class IncomeRange extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: 1200,
      multiplier: 1,
      stepSize: 25000
    }
  }

  // static getDerivedStateFromProps (nextProps, prevState) {
  //   let multiplier = 1
  //   let stepSize = 25000
  //   if (nextProps.income_type === 'salaried') {
  //     multiplier = 12
  //     stepSize = 1000
  //   }
  //   let value = nextProps.income_range ? Math.ceil(nextProps.income_range / multiplier / stepSize) : 1
  //   return {
  //     value,
  //     multiplier,
  //     stepSize
  //   }
  // }

  render() {
    //<input type="range" min="1" max="10000" value={this.state.value}  onChange={e=>{this.setState({value : e.target.value})}} />

    return (
      <div>
        <h1>Your Income</h1>
        <h2>{this.props.income_type === 'salaried' ? 'Please Enter your monthly take home salary' : 'Please Enter your yearly income'}</h2>
        <h3>{'₹' + this.state.value}</h3>
        <div>
          <input type="number" placeholder={this.props.income_type === 'salaried' ? 'Your monthly income' : 'Your yearly income'} onChange={e=>{
            if(this.props.income_type === 'salaried'){
              this.setState({value : e.target.value * 12})
            }else{
              this.setState({value : e.target.value})
            }
          }} />
        </div>
        <button className="btn btn-primary" onClick={() => {
          if (this.state.value !== null) {
            this.props.onChange({income_range: this.state.value})
          }
        }} > next
        </button>
      </div>
    )
  }
}
