import React, {Component} from 'react';
import {
  VirtualizedList,
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Platform
} from 'react-native';
import {
  RkCard, RkStyleSheet,
  RkText,
  RkButton
} from 'react-native-ui-kitten';
import {Avatar} from './avatar';

import {
  KittenTheme
} from '../config/theme'

import { scaleVertical, scale } from '../utils/scale';

export default class OffersView extends Component {
  constructor(props) {
    super(props)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.offers.length !== this.props.offers.length) {
      return true
    }
    for (let i = 0, len = nextProps.offers.length; i < len; i++) {
      if (nextProps.offers[i].id !== this.props.offers[i]) {
        return true
      }
    }
    if (nextProps.canOpenSettings !== this.props.canOpenSettings || nextProps.locationPermissionType !== this.props.locationPermissionType || nextProps.locationPermissionStatus !== this.props.locationPermissionStatus || nextProps.onRequestLocationPermission !== this.props.onRequestLocationPermission) {
      return true
    }
    return false
  }

  _renderItem ({item}) {
    if (item.merchant_id) {
      return <TouchableOpacity delayPressIn={35} activeOpacity={0.8}
        // onPress={() => 
        //   this.props.navigation.navigate('Article', {id: info.item.id
        // })}
        >
        <RkCard rkType='offer' style={styles.card}>
          <Image rkCardImg source={item.photo}/>
          <View rkCardHeader style={styles.content}>
            <RkText style={styles.section} rkType='header4'>{item.intro}</RkText>
          </View>
          <View rkCardContent>
            <View>
              <RkText rkType='primary3 mediumLine' numberOfLines={2}>{item.title}</RkText>
            </View>
          </View>
          <View rkCardFooter>
            <View style={styles.userInfo}>
              <RkText rkType='header6'>{item.merchant_name}</RkText>
            </View>
            <RkButton rkType='clear'>Get Details</RkButton>
          </View>
        </RkCard>
      </TouchableOpacity>
    } else {
      return <TouchableOpacity delayPressIn={35} activeOpacity={0.8}
        // onPress={() => 
        //   this.props.navigation.navigate('Article', {id: info.item.id
        // })}
        >
        <RkCard rkType='offer' style={styles.card}>
          <Image rkCardImg source={item.photo}/>
          <View rkCardHeader style={styles.content}>
            <RkText style={styles.section} rkType='header4'>{item.intro}</RkText>
          </View>
          <View rkCardContent>
            <View>
              <RkText rkType='primary3 mediumLine' numberOfLines={2}>{item.title}</RkText>
            </View>
          </View>
        </RkCard>
      </TouchableOpacity>
    }
  }

  _renderOffers () {
    if (this.props.offers && this.props.offers.length > 0) {
      return (
        <View rkCardContent>
          <VirtualizedList data={this.props.offers}
            getItemCount={() => this.props.offers ? this.props.offers.length : 0}
            getItem={(data, index) => data[index]}
            keyExtractor={(item) => ''+item.id}
            horizontal={true}
            ItemSeparatorComponent={() => <View style={{marginRight: scale(16)}}/>}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            directionalLockEnabled
            renderItem={this._renderItem} />
          <RkButton rkType='clear' style={{flex: 1, flexDirection: 'row'}}>See All Offers</RkButton>
        </View>
      )
    } else {
      return (
        <View rkCardContent>
          <RkText style={styles.location_helper_text} rkType='header2'>No Offers Around You</RkText>
          <RkText style={styles.location_helper_text} rkType='secondary3'>We don't seem to have any offers around you right now. Check back soon!</RkText>
        </View>
      )
    }
  }

  render() {
    return (
      <RkCard rkType='homeCard' style={[this.props.style]}>
        {this._renderOffers()}
      </RkCard>
    )
  }
}

let styles = StyleSheet.create({
  container: {
    marginHorizontal: 0 - scale(16),
    paddingVertical: scaleVertical(8)
  },
  userInfo: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  avatar: {
    marginRight: scale(16)
  },
  location_helper_text: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center'
  },
  helper_details: {
    marginTop: scaleVertical(16)
  },
  secondary_helper_details: {
    marginTop: scaleVertical(4)
  }
})