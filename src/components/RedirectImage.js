import React from 'react'

import {
  Image,
  View
} from 'react-native'

class RedirectImage extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      uri: ''
    }
  }

  static shouldComponentUpdate(nextProps, nextState) {
    return nextState.uri !== this.state.uri || nextProps.source.uri !== this.props.source.uri
  }

  async getRedirect(source) {
    let {uri, ...options} = source
    const response = await fetch(uri, ...options)
    if ((response.status & 200) === 200) {
      const data = await response.json()
      return data.url
    }
  }

  componentDidUpdate() {
    if (this.props.source && this.props.source.uri) {
      this.getRedirect(this.props.source).then((uri)=>{
        this.setState({uri: uri})
      })
    }
  }

  render() {
    if (this.state.uri) {
      return <Image {...this.props} source={{uri: this.state.uri}} />
    } else {
      return <View {...this.props} />
    }
  }
}

// RedirectImage.propTypes = {
//   uri: PropTypes.string.isRequired,
// }

export default RedirectImage