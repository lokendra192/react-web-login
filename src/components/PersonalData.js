import React, { Component } from 'react';

// import {
//   View,
//   FlatList,
//   Dimensions,
//   StyleSheet
// } from 'react-native';

import {NameEmailReferrer} from './NameEmailReferrer'
import {IncomeType} from './IncomeType'
import {IncomeRange} from './IncomeRange'
import {CibilAndDefault} from './CibilAndDefault'
import { MarriedAndKids } from './MarriedAndKids';

export class PersonalData extends Component {
  constructor(props) {
    super(props)

    // this.state = {
    //   name: null,
    //   email: null,
    //   referrer: null,
    //   from_where: null,
    //   income_type: null,
    //   income_range: null,
    //   married: null,
    //   kids: null,
    //   cibil: null,
    //   default_recent: null,
    //   loading: false
    // }

    this.state = {
      name: 'lokendra',
      email: 'lavcool9999@gmail.com',
      referrer: 'gunjeet singh',
      from_where: 'youtube ,blogs',
      income_type: 'salaried',
      income_range: 0,
      married: true,
      kids: 'none',
      cibil: false,
      default_recent: false,
      loading: false,
      index : 0
    }
    let sliderValue = null;
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    return {
      loading: nextProps.loading
    }
  }

  _renderItem = (item) => {
    // let {width} = Dimensions.get('window');
    return (
      <div className="container">
        {item}
      </div>
    )
  }

  scrollToIndex(index){
    let items = this.getItems();
    return items[index];
  }

  getItems () {
    // use the states to determine the children
    let items = []
    items.push(<NameEmailReferrer name={this.state.name} email={this.state.email} referrer={this.state.referrer} from_where={this.state.from_where} onChange={(data) => {
      this.setState(data);
      console.log(data);
      console.log(this.state);
      this.setState({index : 1})
      // this.flatListRef.scrollToIndex({animated: true, index: '1'})
      this.scrollToIndex(1);
    }} />)
    items.push(<IncomeType onBack={() => this.flatListRef.scrollToIndex({animated: true, index: '0'})} income_type={this.state.income_type} onChange={(data) => {
      this.setState(data)
      this.setState({index : 2})
      // this.flatListRef.scrollToIndex({animated: true, index: '2'})
    }} />)
    items.push(<IncomeRange onBack={() => this.flatListRef.scrollToIndex({animated: true, index: '1'})} income_type={this.state.income_type} income_range={this.state.income_range} onChange={(data) => {
      this.setState(data)
      this.setState({index : 3})
      // this.flatListRef.scrollToIndex({animated: true, index: '3'})
    }} />)
    items.push(<MarriedAndKids onBack={() => this.flatListRef.scrollToIndex({animated: true, index: '2'})} married={this.state.married} kids={this.state.kids} onChange={(data) => {
      this.setState(data)
      this.setState({index : 4})
      // this.flatListRef.scrollToIndex({animated: true, index: '4'})
    }} />)
    items.push(<CibilAndDefault onBack={() => this.flatListRef.scrollToIndex({animated: true, index: '3'})} loading={this.state.loading} cibil={this.state.cibil} default_recent={this.state.default_recent} onChange={(data) => {
      this.props.onDone({...this.state, ...data})
    }} />)
    return items
  }

  render() {
    // let items = this.getItems()
    // let view = scrollToIndex
    return (
      <div>
        {this.scrollToIndex(this.state.index)}
      </div>
    )
  }
}
