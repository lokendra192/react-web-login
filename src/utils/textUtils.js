function formatNumber(num) {
  return num > 999 ? (num/1000).toFixed(1) + 'k' : num
}

const INR_FORMAT_WITHOUT_SYMBOL = {
  format: function (number) {
    // let format = []
    // if ((number * 100) % 100 !== 0) {
    //   number = parseInt(number * 100) / 100
    // } else {
    //   number = parseInt(number)
    // }
    return ''+number
    // number = parseInt(number)
    // let i = 0
    // while (number > 0) {
    //   if (i == 1) {
    //     continue
    //   }
    //   if (i % 2 === 1) {
    //     format.unshift(',')
    //   }
    //   i = i + 1
    //   format.unshift(number % 10)
    //   number = parseInt(number / 10)
    // }
    // return format.join('')
  }
}

export default formatNumber;

export {INR_FORMAT_WITHOUT_SYMBOL}