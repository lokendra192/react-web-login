import React, { Component } from 'react'

let _location
let _watchId = null

export function startLocationTracking (listener) {
  if (_watchId != null) {
    return
  }
  navigator.geolocation.getCurrentPosition((location) => {
    _location = location
  }, {
    maximumAge: 10000,
    enableHighAccuracy: true
  })
  _watchId = navigator.geolocation.watchPosition((location) => {
    _location = location
  }, {
    maximumAge: 10000,
    enableHighAccuracy: true,
    distanceFilter: 50000,
    useSignificantChanges: true
  })
}

export function stopLocationTracking () {
  if (_watchId) {
    navigator.geolocation.clearWatch(_watchId)
  }
}

// import BackgroundGeolocation from 'react-native-mauron85-background-geolocation'

// const resetConfig = () => {
//   BackgroundGeolocation.configure({
//     desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
//     stationaryRadius: 50,
//     distanceFilter: 50,
//     fastestInterval: 5000,
//     activitiesInterval: 30000,
//     debug: __DEV__,
//     startOnBoot: true,
//     stopOnTerminate: false,
//     locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
//     interval: 10000
//   })
// }

// export function startLocationTracking(listener) {
//   BackgroundGeolocation.start()
//   BackgroundGeolocation.on('start', (location) => {
//     _location = location
//     if (listener) {
//       listener.call(this, location)
//     }
//   })
// }

// export function stopLocationTracking() {
//   BackgroundGeolocation.stop()
//   BackgroundGeolocation.events.forEach(event => BackgroundGeolocation.removeAllListeners(event))
// }

export function getLastLocation() {
  return _location
}